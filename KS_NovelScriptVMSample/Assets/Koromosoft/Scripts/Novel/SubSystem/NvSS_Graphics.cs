﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace K5.Novel
{
    class NvInst
    {
        public NvInst(GameObject inst, int depth) { Inst = inst; Depth = depth; }
        public GameObject Inst { get; set; } = null;
        public int Depth { get; set; } = 0;
    }

    //グラフィクス関係のサブシステム
    public class NvSS_Graphics : NvSS_Base
    {
        //デフォルトの親オブジェクト
        [SerializeField]
        private Transform _ImgParent = null;

        //画像ファイルのルートパス
        [SerializeField]
        private string _ImgRootPath = "Prefabs/Novel/";

        //生成したインスタンスリスト
        private Dictionary<int, NvInst> _Insts = new Dictionary<int, NvInst>();

        //インスタンスハンドルの内部カウント
        private int _InstHandleCount = 0;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        //プレハブの生成
        public int NewPrefab(int pc, string prefabName, string parentName)
        {
            var prefab = Resources.Load(_ImgRootPath + prefabName) as GameObject;
            if(prefab == null)
            {
                NovelDebug.LogError(pc, "プレハブが見つかりません。パスを確認してください。" + prefabName);
                return -1;
            }

            var inst = Instantiate(prefab);

            //NvPrefabが見つからない場合は破棄する
            var comp = inst.GetComponent<NvImgPrefab>();
            if (comp == null)
            {
                Destroy(inst);
                NovelDebug.LogError(pc, "newコマンドで生成するプレハブはNvPrefabコンポーネントを所持している必要があります。");
                return -1;
            }

            //消しておく
            comp.SetAlpha(0.0f);

            var parent = (parentName == "") ? _ImgParent : _ImgParent.Find(parentName);
            if(parent != null)
            {
                inst.transform.SetParent(parent.transform, false);
            }

            var handle = _InstHandleCount;
            _Insts.Add(_InstHandleCount++, new NvInst(inst,0));

            return handle;
        }

        //プレハブの削除
        public void DeletePrefab(int handle)
        {
            if(_Insts.ContainsKey(handle))
            {
                Destroy(_Insts[handle].Inst);
                _Insts.Remove(handle);
            }
        }

        //インスタンスの視覚化
        public IEnumerator ShowPrefab(int pc, int handle, Vector2 position, int depth, float time)
        {
            if(_Insts.ContainsKey(handle))
            {
                var comp = _Insts[handle].Inst.GetComponent<NvImgPrefab>();
                _Insts[handle].Inst.SetActive(true);

                //位置の設定
                var rect_comp = _Insts[handle].Inst.GetComponent<RectTransform>();
                rect_comp.anchoredPosition = position;

                //重ね順の設定
                _Insts[handle].Depth = depth;
                var list = new List<NvInst>();
                foreach(var i in _Insts)
                {
                    list.Add(i.Value);
                }
                list.OrderBy(x => x.Depth);
                foreach(var l in list)
                {
                    l.Inst.transform.SetAsLastSibling();
                }

                //表示処理
                var progress = 0.0f;
                while(progress < 1.0f)
                {
                    progress += Time.deltaTime/ time;
                    comp.SetAlpha(progress);
                    yield return new WaitForEndOfFrame();
                }
                comp.SetAlpha(1.0f);
            }
            yield return null;
        }

        public IEnumerator ShowPrefab(int pc, int handle, float time)
        {
            if(_Insts.ContainsKey(handle))
            {
                var inst = _Insts[handle];
                yield return ShowPrefab(pc, handle, inst.Inst.GetComponent<RectTransform>().anchoredPosition, inst.Depth, time);
            }
            yield return null;
        }

        //インスタンスを消す（メモリ上には残ったまま）
        public IEnumerator HidePrefab(int pc, int handle, float time)
        {
            if (_Insts.ContainsKey(handle))
            {
                var comp = _Insts[handle].Inst.GetComponent<NvImgPrefab>();

                //隠す
                var progress = 1.0f;
                while (progress > 0.0f)
                {
                    progress -= Time.deltaTime / time;
                    comp.SetAlpha(progress);
                    yield return new WaitForEndOfFrame();
                }
                comp.SetAlpha(0.0f);
            }
            yield return null;
        }

        //拡大
        public IEnumerator Scale(int pc, int handle, Vector2 size, float time)
        {
            if (_Insts.ContainsKey(handle))
            {
                var comp = _Insts[handle].Inst.GetComponent<RectTransform>();

                var sourceSize = comp.localScale;
                var targetSize = comp.localScale * size;

                //拡大縮小する
                var progress = 0.0f;
                while (progress < 1.0f)
                {
                    progress += Time.deltaTime / time;

                    var vec = new Vector2();
                    vec.x = Mathf.Lerp(sourceSize.x, targetSize.x, progress);
                    vec.y = Mathf.Lerp(sourceSize.y, targetSize.y, progress);
                    comp.localScale = vec;
                    yield return null;
                }
                comp.localScale = targetSize;
            }
            yield return null;
        }

        //移動
        public IEnumerator Move(int pc, int handle, Vector2 delta, float time)
        {
            if (_Insts.ContainsKey(handle))
            {
                var comp = _Insts[handle].Inst.GetComponent<RectTransform>();

                var pos1 = comp.anchoredPosition;
                var pos2 = comp.anchoredPosition + delta;

                //拡大縮小する
                var progress = 0.0f;
                while (progress < 1.0f)
                {
                    progress += Time.deltaTime / time;

                    var vec = new Vector2();
                    vec.x = Mathf.Lerp(pos1.x, pos2.x, progress);
                    vec.y = Mathf.Lerp(pos1.y, pos2.y, progress);
                    comp.anchoredPosition = vec;
                    yield return null;
                }
                comp.anchoredPosition = pos2;
            }
            yield return null;
        }

        //回転
        public IEnumerator Rotate(int pc, int handle, float angle, float time)
        {
            if (_Insts.ContainsKey(handle))
            {
                var comp = _Insts[handle].Inst.GetComponent<RectTransform>();

                var angle1 = comp.rotation.eulerAngles.z;
                var angle2 = comp.rotation.eulerAngles.z + angle;

                //拡大縮小する
                var progress = 0.0f;
                while (progress < 1.0f)
                {
                    progress += Time.deltaTime / time;
                    
                    var angleZ = Mathf.Lerp(angle1, angle2, progress);
                    var rot = comp.rotation.eulerAngles;
                    rot.z = angleZ;
                    comp.rotation = Quaternion.Euler(rot);
                    yield return null;
                }
                var rot2 = comp.rotation.eulerAngles;
                rot2.z = angle2;
                comp.rotation = Quaternion.Euler(rot2);
            }
            yield return null;
        }

        //フェード

        //fxクリア

       
    }
}