﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace K5
{
    public static class FileUtility
    {
        //UnityのリソースフォルダからCSVをロードする
        public static List<List<string>> LoadCSVFileFromResources(string path)
        {
            var outList = new List<List<string>>();

            var rowData = Resources.Load(path) as TextAsset;
            if(!rowData)
            {
                Debug.LogError("ファイルの読み込みに失敗しました。:" + path);
                return null;
            }

            var sr = new StringReader(rowData.text);

            while(sr.Peek() != -1)
            {
                var line = sr.ReadLine();
                var unitList = SplitComma(line);
                unitList.RemoveAll((s) => { return s == ""; }); //空行は削除

                outList.Add(unitList);
            }

            return outList;
        }

        //文字列をカンマ区切りにするが、""で囲った中は無視する
        private static List<string> SplitComma(string data)
        {
            bool inText = false; //現在文字列の中にいるかのフラグ
            var list = new List<string>();
            string part = ""; //listにaddするようのパーツ

            for(int i=0; i<data.Length; i++)
            {
                char c = data[i];
                //二重引用符が出たらトグルする
                //二重引用符自体は出力に含めるのでcontinueしない
                if (c == '\"')
                { 
                    inText = !inText;
                }
                else if(c == ',' && !inText)
                {
                    //カンマがでて、なおかつテキストの中じゃない場合はスプリットする
                    list.Add(part);
                    part = "";
                    continue;
                }

                part += c;
            }
            list.Add(part);

            return list;
        }
    }
}
