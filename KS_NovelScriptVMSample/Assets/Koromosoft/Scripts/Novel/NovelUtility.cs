﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace K5.Novel
{
    public static class NvConvert
    {
        //文字列型への変換
        public static string ToStr(object source, int pc, int argID)
        {
            return source.ToString();
        }
        //int型への変換
        public static int ToInt(object source, int pc, int argID)
        {
            try { return (int)source; }
            catch (InvalidCastException)
            {
                throw new NovelVMException(pc, "[arg" + argID.ToString() + "]のint型への変換に失敗しました。");
            }
        }
        //レジスタ型への変換
        public static NvRegister ToReg(object source, int pc, int argID)
        {
            try { return (NvRegister)source; }
            catch(InvalidCastException)
            {
                throw new NovelVMException(pc, "[arg" + argID.ToString() + "]のregister型への変換に失敗しました。");
            }
        }
    }
}
