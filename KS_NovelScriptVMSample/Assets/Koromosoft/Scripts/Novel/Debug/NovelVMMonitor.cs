﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace K5.Novel
{
    public class NovelVMMonitor : MonoBehaviour
    {
        [SerializeField]
        private NovelEngine _Engine = null;

        [SerializeField]
        private GameObject _TextPrefab = null;

        [SerializeField]
        private Transform _RegParent = null;
        [SerializeField]
        private Transform _TmRegParent = null;
        [SerializeField]
        private Transform _SpRegParent = null;

        private List<GameObject> _RegInsts = new List<GameObject>();
        private List<GameObject> _TmRegInsts = new List<GameObject>();
        private List<GameObject> _SpRegInsts = new List<GameObject>();

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void OnClick_Update()
        {
            //前回分の削除処理
            foreach (var i in _RegInsts) Destroy(i);
            _RegInsts.Clear();

            //レジスタの格納
            var vm = _Engine.VM;
            for(int i=0; i<vm.Reg.Length; i++)
            {
                var regdat = (vm.Reg[i].Data == null) ? "null" : vm.Reg[i].Data.ToString();
                var inst = Instantiate(_TextPrefab);
                inst.transform.SetParent(_RegParent,false);
                
                inst.GetComponent<Text>().text = i.ToString() + ":" + regdat;
                _RegInsts.Add(inst);
            }
        }
    }
}