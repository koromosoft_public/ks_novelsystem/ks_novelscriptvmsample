﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;


namespace K5.Novel
{
    //グラフィクス関係のサブシステム
    public class NvSS_Text : NvSS_Base
    {
        [SerializeField]
        private NvWindow _Window = null;
        [SerializeField]
        private float _TextSpeed = 10.0f;

        [SerializeField]
        private GameObject _NextIcon = null;

        //入力街の状態かどうか
        private bool isWaitClick = false;
        //次へ行くフラグ
        private bool isNext = false;

        //名前変更
        public void ChangeName(string name)
        {
            _Window.SetName(name);
        }

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.Return)||Input.GetMouseButtonDown(0))
            {
                if (isWaitClick) isNext = true;
                else isWaitClick = true;
            }

            if (isWaitClick)
            {
                _NextIcon.SetActive(true);
            }
            else
                _NextIcon.SetActive(false);
        }

        //テキスト表示
        //本ゲームは１行１コンテナの作りなので、
        //showコマンドと同時にスキンを生成して、スクロールリストにプッシュする
        public IEnumerator ShowText(string str)
        {
            var baseText = _Window.GetText();

            _Window.Show();

            //一文字ずつ表示する
            for(int i=0; i<str.Length; i++)
            {
                //クリックされたら全文表示の状態にする
                if (isWaitClick) break;

                var substr = baseText + str.Substring(0, i);
                _Window.SetText(substr);
                yield return new WaitForSeconds(1.0f / _TextSpeed);
            }
            _Window.SetText(baseText + str + "\n");
            
            yield return null;
        }

        //改ページ
        public IEnumerator NewPage()
        {
            isWaitClick = true;

            //クリックまち処理   
            while (!isNext) yield return null;

            _Window.SetText("");
            isNext = false;
            isWaitClick = false;

            yield return null;
        }

        //ウィンドウを表示する
        public void WinOpen()
        {
            _Window.Show();
        }

        //ウィンドウを閉じる
        public void WinClose()
        {
            _Window.Hide();
        }

    }
}