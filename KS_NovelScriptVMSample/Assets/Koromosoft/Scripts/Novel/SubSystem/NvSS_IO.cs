﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace K5.Novel
{
    public class NvSS_IO : NvSS_Base
    {
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        //デバッグ用出力機能
        public void Print(string str)
        {
            Debug.Log(str);
        }
    }
}