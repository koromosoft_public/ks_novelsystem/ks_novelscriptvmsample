﻿using System;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace K5.Novel
{
    //型タイプ
    public enum ENvType :int
    {
        num,
        str,
        reg,
        adr,
        _Num
    }
    //特殊レジスタタイプ
    public enum ENvSpRegType :int
    {
        pc,
        sp,
        sg,
        ra,
        _Num
    }

    //レジスタクラス
    public class NvRegister
    {
        public ENvType Type { get; set; } = ENvType.num;
        public object Data { get; set; } = 0;
    }

    //コマンドクラス用の属性
    [AttributeUsage(AttributeTargets.Class)]
    public class NvCommandAttribute : Attribute
    {
        public int id { get; set; } = -1;
        public bool syncEnable { get; set; } = false; //同期命令が有効かどうか
    }

    //コマンドクラス
    //このクラスを継承して、各コマンドを作ってください。
    //派生クラスは、必ずsealedクラスにしてください。
    public abstract class NvCommand
    {
        //実行するプロセスを記述する
        public abstract IEnumerator Process(NovelVM vm, List<object> args);
    }

    //プログラムの一行単位の構造体
    public class NvOperation
    {
        public int ID { get; set; } = 0;
        public List<ENvType> ArgType { get; set; } = new List<ENvType>();
        public List<string> Args { get; set; } = new List<string>();
    }

    //VMの例外用クラス
    public class NovelVMException : Exception
    {
        NovelVMException() { }
        public NovelVMException(int pc, string msg)
            : base("[pc]:"+pc.ToString()+" [msg]:" + msg)
        {
            NovelDebug.LogError(pc, msg);
        }
    }

    //ノベルVM
    public class NovelVM
    {
        //レジスタサイズ
        [SerializeField]
        private int _RegSize = 128;
        //汎用レジスタサイズ
        [SerializeField]
        private int _TmpRegSize = 16;

        //プログラムカウンタ
        public int PC { get { return (int)SpReg[(int)ENvSpRegType.pc].Data; } set { SpReg[(int)ENvSpRegType.pc].Data = value; } }
        //一般レジスタ
        public NvRegister[] Reg { get; set; }
        //一時レジスタ
        public NvRegister[] TmpReg { get; set; }
        //特殊レジスタ
        public NvRegister[] SpReg { get; set; }
        //ラベルリスト
        private Dictionary<string, int> _LabelList = new Dictionary<string, int>();
        //スタック
        public Stack<object> Stack { get; set; } = new Stack<object>();
        //同期フラグ
        public bool IsSync { get; set; } = false;
        //動機メソッド
        public List<IEnumerator> SyncMethod { get; set; } = new List<IEnumerator>();
        //終了フラグ exit命令用
        public bool IsEnd { get; set; } = false;

        //プログラムバッファ
        private List<NvOperation> _ProgramBuffer = new List<NvOperation>();
        //コマンドバッファ(実体）
        private Dictionary<int,NvCommand> _CmdBuffer = new Dictionary<int, NvCommand>();
        //コマンドの型データ
        private List<NvOperation> _CmdTypeList = new List<NvOperation>();
        //同期が無効な命令リスト
        private List<int> _IgnoreSyncOprList = new List<int>();

        //ノベルエンジンへの参照
        public NovelEngine Engine { get; private set; } = null;

        public NovelVM(List<NvOperation> cmdTypeList, NovelEngine engine)
        {
            //コマンドクラスの派生情報を取得して、コマンドリストとして格納する
            var allCmdTypes = System.Reflection.Assembly.
                GetAssembly(typeof(NvCommand)).GetTypes().
                Where(t => { return t.IsSubclassOf(typeof(NvCommand)); });

            foreach (var t in allCmdTypes)
            {
                var att = (NvCommandAttribute)Attribute.GetCustomAttribute(t, typeof(NvCommandAttribute));
                _CmdBuffer.Add(att.id, (NvCommand)Activator.CreateInstance(t));
                if (!att.syncEnable) _IgnoreSyncOprList.Add(att.id); //同期無効命令の場合はリストへ記帳
            }

            //コマンドの型データを読み込む
            _CmdTypeList = cmdTypeList;

            //ノベルエンジン取得
            Engine = engine;
            
        }

        public void Init()
        {
            //レジスタ領域の確保
            Reg = new NvRegister[_RegSize]; for (var i = 0; i < _RegSize; i++) Reg[i] = new NvRegister();
            TmpReg = new NvRegister[_TmpRegSize];
            SpReg = new NvRegister[(int)ENvSpRegType._Num];

            //初期化処理
            for (var i = 0; i < _RegSize; i++) Reg[i] = new NvRegister();
            for (var i = 0; i < _TmpRegSize; i++) TmpReg[i] = new NvRegister();
            for (var i = 0; i < (int)ENvSpRegType._Num; i++) SpReg[i] = new NvRegister();

            //プログラムカウンタリセット
            PC = 0;

            //ラベルリスト、プログラムリスト、スタック初期化
            _LabelList = new Dictionary<string, int>();
            _ProgramBuffer = new List<NvOperation>();
            Stack = new Stack<object>();
            SyncMethod = new List<IEnumerator>();
            IsSync = false;
            IsEnd = false;
        }

        public IEnumerator Execute(List<List<string>> program, Action callback)
        {
            //初期化
            Init();

            //ラベルリストを構成
            var index = 0;
            for(;index<program.Count;index++)
            {
                if (program[index][0] == "label:") { index++; break; }
            }
            for(;index<program.Count;index++)
            {
                if(program[index][0] == "prog:") { index++; break; }
                _LabelList.Add(program[index][0], int.Parse(program[index][1]));
            }

            //プログラムを読み出し
            for(;index<program.Count;index++)
            {
                //コマンド情報を構築
                var ope = new NvOperation();
                ope.ID = int.Parse(program[index][0]);
                ope.ArgType = new List<ENvType>();
                ope.Args = new List<string>();
                //引数の読み出し
                for(int i=1; i<program[index].Count; i+=2)
                {
                    ope.ArgType.Add((ENvType)Enum.Parse(typeof(ENvType),program[index][i]));
                    ope.Args.Add(program[index][i + 1]);
                }
                _ProgramBuffer.Add(ope);
            }

            //実行開始
            for (; PC < _ProgramBuffer.Count; PC++)
            {
                var args = new List<object>();

                //引数を変換して格納
                var refelenceFormats = _CmdTypeList.Find(x => x.ID == _ProgramBuffer[PC].ID).ArgType;
                var argCount = Math.Min(refelenceFormats.Count, _ProgramBuffer[PC].Args.Count);
                for (int i = 0; i < argCount; i++)
                {
                    try
                    {
                        var source = refelenceFormats[i];
                        var dest = _ProgramBuffer[PC].ArgType[i];
                        var arg_str = _ProgramBuffer[PC].Args[i];

                        args.Add(ConvertType(arg_str, source, dest));
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        NovelDebug.LogError(PC, "");
                    }
                }


                NvCommand cmd = new NvCmd_Nil();
                try
                {
                    cmd = _CmdBuffer[_ProgramBuffer[PC].ID];
                }
                catch (KeyNotFoundException)
                {
                    NovelDebug.Log(PC, "指定されているIDのコマンドが実装されていません。");
                }

                //コマンド呼び出し
                //同期が有効の場合はawaitまで待つ
                if (!IsSync || _IgnoreSyncOprList.Contains(_ProgramBuffer[PC].ID))
                    yield return cmd.Process(this, args);
                else
                    SyncMethod.Add(cmd.Process(this, args));

                //終了
                if (IsEnd)
                {
                    callback?.Invoke();
                    yield break;
                }
            }
            callback?.Invoke();
            yield return null;
        }

        //型チェックと変換メソッド
        public object ConvertType(string arg_str, ENvType source, ENvType dest)
        {
            //下記の通り変換
            //リファレンス：数値or文字列    実測値：レジスタ　→　数値or文字列
            //リファレンス：数値            実測値：文字列　  →　エラー
            //リファレンス：文字列          実測値：数字　    →　文字列
            //リファレンス：レジスタ        実測値：数字or文字列　→　エラー
            //リファレンス：アドレス        実測値：全部 →　数字（アドレス）

            if (source == dest)
            {
                if (dest == ENvType.num)
                    return (int.Parse(arg_str));
                else if (dest == ENvType.reg)
                    return (RegToReg(arg_str));
                else if (dest == ENvType.adr)
                    return (_LabelList[arg_str]);
                else
                    return (arg_str.Replace("\"",""));
            }
            //レジスタから数値に変換
            else if (source == ENvType.num && dest == ENvType.reg)
            {
                return (RegToInt(arg_str));
            }
            //レジスタから文字列に変換
            else if (source == ENvType.str && dest == ENvType.reg)
            {
                return (RegToStr(arg_str));
            }
            //数字から文字列に変換（そのまま）
            //アドレスから文字列に変換（そのまま）
            else if ((source == ENvType.str && dest == ENvType.num) ||
                    source == ENvType.str && dest == ENvType.adr)
            {
                return (arg_str);
            }
            //数字からアドレスに変換
            else if (source == ENvType.adr && dest == ENvType.num)
            {
                return (int.Parse(arg_str));
            }
            //レジスタからアドレスに変換（ラベル名として変換）
            else if (source == ENvType.adr && dest == ENvType.reg)
            {
                return (_LabelList[RegToStr(arg_str)]);
            }
            //文字列からアドレスに変換
            else if (source == ENvType.adr && dest == ENvType.str)
            {
                return (_LabelList[arg_str]);
            }
            else
            {
                throw new NovelVMException(PC, "引数の型が間違っています。[正しい型]:" + source.ToString() + "[指定された型]:" + dest.ToString());
            }
        }

        #region レジスタ文字列変換メソッド
        //文字列で表されたレジスタをstr型に変換
        public string RegToStr(string reg)
        {
            var regDat = RegToReg(reg);
            return regDat.Data.ToString();
        }

        //文字列で表されたレジスタをint型に変換
        public int RegToInt(string reg)
        {
            var regDat = RegToReg(reg);

            try
            {
                return Convert.ToInt32(regDat.Data);
            }
            catch (FormatException)
            {
                throw new NovelVMException(PC, "レジスタがnum型ではありません。:" + reg);
            }
        }

        //文字列で表されたレジスタを実際のレジスタオブジェクトに変換
        public NvRegister RegToReg(string reg)
        {
            //エラー処理
            if(reg=="")
            {
                throw new NovelVMException(PC, "文字列が空です。");
            }
            if(reg[0] != '@')
            {
                throw new NovelVMException(PC, "文字列がレジスタではありません。");
            }

            //2文字目以降が文字か数字かでレジスタの種類を分岐
            var match_gen = Regex.Match(reg, "^@[0-9]+$");
            var match_spe = Regex.Match(reg, "^@[a-z][a-z](|[0-9]+)$");

            //汎用レジスタ
            if (match_gen.Success)
            {
                var num = int.Parse(match_gen.Value.Replace("@", ""));
                try
                {
                    return Reg[num];
                }
                catch(IndexOutOfRangeException)
                {
                    throw new NovelVMException(PC, "範囲外の汎用レジスタが参照されました。汎用レジスタは0-" + _RegSize.ToString() + "の範囲である必要があります。");
                }
            }
            //特殊レジスタ
            else if (match_spe.Success)
            {
                //特殊レジスタの2文字を読み出す
                string key = reg[1].ToString() + reg[2].ToString();
                switch(key)
                {
                    //プログラムカウンタ
                    case "pc": return SpReg[(int)ENvSpRegType.pc];
                    //スタックポインタ
                    case "sp": return SpReg[(int)ENvSpRegType.sp];
                    //符号レジスタ
                    case "sg": return SpReg[(int)ENvSpRegType.sg];
                    //リターンアドレス
                    case "ra": return SpReg[(int)ENvSpRegType.ra];

                    //一時レジスタ
                    case "tm":
                        var num = 0;
                        try
                        {
                            num = int.Parse(reg.Substring(3));
                        }
                        catch (ArgumentOutOfRangeException)
                            { throw new NovelVMException(PC, "一時レジスタの番号が指定されていません。:" + reg); }
                        catch (FormatException)
                            { throw new NovelVMException(PC, "汎用レジスタの書式が違います。tmのあとには数字を指定してください。" + reg); }

                        try
                        {
                            return TmpReg[num];
                        }
                        catch(ArgumentOutOfRangeException)
                            { throw new NovelVMException(PC, "一時レジスタの番号が範囲外です。0-" + (_TmpRegSize-1).ToString() + "の範囲で指定してください。"); }

                    //それ以外はエラー
                    default:
                        throw new NovelVMException(PC, "特殊レジスタの識別子が正しくありません。形式を見直してください。:" + reg);
                }
            }
            //それ以外はエラー
            else
            {
                throw new NovelVMException(PC, "レジスタの形式が正しくありません。形式を見直してください。:" + reg);
            }
        }
        
        #endregion
    }

    //ノベルシステムの書式に合わせたデバッグメッセージクラス
    public static class NovelDebug
    {
        public static void LogError(int pc, string msg)
        {
            Debug.LogError("[pc]:" + pc.ToString() + " [msg]:" + msg);
        }

        public static void Log(int pc, string msg)
        {
            Debug.Log("[pc]:" + pc.ToString() + " [msg]:" + msg);
        }
    }
}