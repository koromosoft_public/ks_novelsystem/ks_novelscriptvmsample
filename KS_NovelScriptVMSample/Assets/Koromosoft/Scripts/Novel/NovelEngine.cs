﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace K5.Novel
{
    public class NovelEngine : MonoBehaviour
    {
        //コマンド型データへのパス
        [SerializeField]
        private string _CmdTypePath = "Scenarios/OprList";

        //サブシステムへの参照
        //入出力サブシステム
        [SerializeField]
        private NvSS_IO _SS_IO = null;
        public NvSS_IO SS_IO { get { return _SS_IO; } }
        //グラフィックサブシステム
        [SerializeField]
        private NvSS_Graphics _SS_Graphics = null;
        public NvSS_Graphics SS_Graphics { get { return _SS_Graphics; } }
        //サウンドサブシステム
        [SerializeField]
        private NvSS_Sound _SS_Sound = null;
        public NvSS_Sound SS_Sound { get { return _SS_Sound; } }
        //テキストシステム
        [SerializeField]
        private NvSS_Text _SS_Text = null;
        public NvSS_Text SS_Text { get { return _SS_Text; } }
        
        //仮想マシン
        public NovelVM VM { get; private set; } = null;

        //ゲームメモリ
        //ホントはフラグとかで使うのだが、このタイトルでは使わなそうなので
        //一時的にここへ作成
        public object[] SystemMemory { get; set; } = new object[128];

        //終了時に呼び出されるコールバック
        public Action EndCallBack { get; set; } = null;

        private void Awake()
        {
            //コマンドの型データを読み出す
            var data = FileUtility.LoadCSVFileFromResources(_CmdTypePath);
            var list = new List<NvOperation>();
            foreach (var d in data)
            {
                //空白を削除
                d.RemoveAll((s) => { return s == ""; });

                var item = new NvOperation();
                item.ID = int.Parse(d[0]);
                for (int i = 2; i < d.Count; i++)
                {
                    //*の場合はその前の型をできるだけ多く積み込む
                    //簡易実装なので堅牢性はない
                    if(d[i] == "*")
                    {
                        for (int j = 0; j < 128; j++) item.ArgType.Add((ENvType)Enum.Parse(typeof(ENvType), d[i-1]));
                    }
                    else
                        item.ArgType.Add((ENvType)Enum.Parse(typeof(ENvType), d[i]));
                }
                //コマンドリストに追加
                list.Add(item);
            }

            //VMの初期化
            VM = new NovelVM(list, this);

        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        //ノベルシーンを実行する
        public void Execute(string scriptFilePath)
        {
            //VM初期化
            VM.Init();

            //データをロード
            var Data = FileUtility.LoadCSVFileFromResources(scriptFilePath);

            //実行
            StartCoroutine(VM.Execute(Data,EndCallBack));
        }

        //パラレルコルーチン実行
        //複数のコルーチンを同時実行し、終了を同期する
        public IEnumerator StartParallelCoroutine(List<IEnumerator> methods, Action onComplete)
        {
            var coroutine = new List<Coroutine>();

            //実行
            foreach(var m in methods)
            {
                coroutine.Add(StartCoroutine(m));
            }

            //待機
            foreach(var c in coroutine)
            {
                yield return c;
            }

            //完了メソッド実行
            onComplete?.Invoke();
        }

    }
}