﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Animator))]
public class NvWindow : MonoBehaviour
{
    [SerializeField]
    private Text _Text = null;
    [SerializeField]
    private Text _Name = null;
    
    private Animator _Anim = null;

    private void Awake()
    {
        _Anim = GetComponent<Animator>();
    }

    //名前をセット
    public void SetName(string name)
    {
        _Name.text = name;
    }

    //テキストをセット
    public void SetText(string str)
    {
        _Text.text = str;
    }

    public string GetText() { return _Text.text; }

    //表示
    public void Show()
    {
        _Anim.SetBool("IsShow", true);
    }

    //非表示
    public void Hide()
    {
        _Anim.SetBool("IsShow", false);
    }
}
