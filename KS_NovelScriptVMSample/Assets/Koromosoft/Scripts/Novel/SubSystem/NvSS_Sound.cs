﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace K5.Novel
{
    public class NvSS_Sound : NvSS_Base
    {
        [SerializeField]
        private AudioSource _BGMSource = null;
        [SerializeField]
        private AudioSource _SESource = null;
        [SerializeField]
        private AudioSource _VoiceSource = null;

        [SerializeField]
        private string _SoundFileBasePath = "Sound/";

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        //BGM再生
        public void PlayBGM(int pc, string path)
        {
            //既存のリソースがあれば開放
            if (_BGMSource.clip != null) Resources.UnloadAsset(_BGMSource.clip);

            var aclip = Resources.Load(_SoundFileBasePath + path) as AudioClip;
            if (aclip == null) NovelDebug.Log(pc, "BGMがみつかりませんでした。パスを確認してください。" + _SoundFileBasePath + path);

            _BGMSource.clip = aclip;
            _BGMSource.Play();
        }

        //BGM再生停止
        public void StopBGM()
        {
            _BGMSource.Stop();

            //既存のリソースがあれば開放
            if (_BGMSource.clip != null) Resources.UnloadAsset(_BGMSource.clip);
        }

        //SE再生
        public void PlaySE(int pc, string path)
        {
            var aclip = Resources.Load(_SoundFileBasePath + path) as AudioClip;
            if (aclip == null) NovelDebug.Log(pc, "SEがみつかりませんでした。パスを確認してください。" + _SoundFileBasePath + path);


            _SESource.clip = aclip;
            _SESource.PlayOneShot(aclip);
        }

        //ボイス再生
        public void PlayVoice(int pc, string path)
        {
            var aclip = Resources.Load(_SoundFileBasePath + path) as AudioClip;
            if (aclip == null) NovelDebug.Log(pc, "Voiceがみつかりませんでした。パスを確認してください。" + _SoundFileBasePath + path);

            _VoiceSource.clip = aclip;
            _VoiceSource.PlayOneShot(aclip);
        }
    }
}