﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace K5.Novel
{
    //ノベルエンジンのサブシステム
    //Unityエンジン側の機能を仲介する役割を持ち
    //エンジン上でインスタンス化されていることが前提
    public class NvSS_Base : MonoBehaviour
    {
        //初期化処理
        public virtual void Init()
        {

        }
    }
}