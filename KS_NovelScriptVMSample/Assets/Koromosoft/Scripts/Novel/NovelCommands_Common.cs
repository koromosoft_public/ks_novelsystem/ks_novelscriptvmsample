﻿using System;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace K5.Novel
{
    /// nil
    /// 何もしないコマンドです。
    [NvCommand(id = 0)]
    sealed class NvCmd_Nil : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            NovelDebug.Log(vm.PC, "[opr]nil");

            yield return null;
        }
    }
    
    /// _label
    /// ラベルコマンド（なにもしない）
    /// @param str ラベル名
    [NvCommand(id = 1)]
    sealed class NvCmd_Label : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            //引数変換
            string arg0 = null;
            try
            {
                arg0 = NvConvert.ToStr(args[0], vm.PC, 0);
            }
            catch(ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //ログ出力
            NovelDebug.Log(vm.PC, "[opr]_label [arg0]str:" + arg0);

            yield return null;
        }
    }

    /// mov
    /// コピーコマンド
    /// いわゆる代入です。
    /// @param reg 代入先レジスタ
    /// @param reg ソースレジスタ
    [NvCommand(id = 2)]
    sealed class NvCmd_MovReg : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            NvRegister arg0 = null;
            NvRegister arg1 = null;

            //引数変換
            try
            {
                arg0 = NvConvert.ToReg(args[0], vm.PC, 0);
                arg1 = NvConvert.ToReg(args[1], vm.PC, 1);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            arg0.Data = arg1.Data;

            //ログ出力
            NovelDebug.Log(vm.PC, "[opr]mov [arg0]reg:" + arg0.ToString() + "[arg1]reg:" + arg1.ToString());

            yield return null;
        }
    }

    /// mov
    /// コピーコマンド
    /// 代入コマンドです。
    /// @param reg 代入先レジスタ
    /// @param str ソースの文字列
    [NvCommand(id = 3)]
    sealed class NvCmd_MovStr : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            NvRegister arg0 = null;
            string arg1 = "";

            //引数変換
            try
            {
                arg0 = NvConvert.ToReg(args[0], vm.PC, 0);
                arg1 = NvConvert.ToStr(args[1], vm.PC, 1);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            arg0.Data = arg1;

            //ログ出力
            NovelDebug.Log(vm.PC, "[opr]mov [arg0]reg:" + arg0.ToString() + "[arg1]str:" + arg1);

            yield return null;
        }
    }

    /// mov
    /// コピーコマンド
    /// 代入コマンドです。
    /// @param reg 代入先レジスタ
    /// @param reg ソースの数値
    [NvCommand(id = 4)]
    sealed class NvCmd_MovNum : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            NvRegister arg0 = null;
            int arg1 = 0;
            
            //引数変換
            try
            {
                arg0 = NvConvert.ToReg(args[0], vm.PC, 0);
                arg1 = NvConvert.ToInt(args[1], vm.PC, 1);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            arg0.Data = arg1;

            NovelDebug.Log(vm.PC, "[opr]_label [arg0]reg:" + arg0.ToString() + "[arg1]num:" + arg1.ToString());

            yield return null;
        }
    }

    /// add
    /// 加算コマンド
    /// 足し算の結果をレジスタに格納します。
    /// @param reg 代入先レジスタ
    /// @param num 左辺
    /// @param num 右辺
    [NvCommand(id = 5)]
    sealed class NvCmd_Add : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            NvRegister arg0 = null;
            int arg1 = 0;
            int arg2 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToReg(args[0], vm.PC, 0);
                arg1 = NvConvert.ToInt(args[1], vm.PC, 1);
                arg2 = NvConvert.ToInt(args[2], vm.PC, 2);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            arg0.Data = arg1 + arg2;

            //ログ出力
            NovelDebug.Log(vm.PC, "[opr]add [arg0]reg:" + arg0.ToString() + "[arg1+2]num:" + arg1.ToString() + "+" + arg2.ToString());

            yield return null;
        }
    }

    /// sub
    /// 減算コマンド
    /// 引き算の結果をレジスタに格納します。
    /// @param reg 代入先レジスタ
    /// @param num 左辺
    /// @param num 右辺
    [NvCommand(id = 6)]
    sealed class NvCmd_Sub : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            NvRegister arg0 = null;
            int arg1 = 0;
            int arg2 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToReg(args[0], vm.PC, 0);
                arg1 = NvConvert.ToInt(args[1], vm.PC, 1);
                arg2 = NvConvert.ToInt(args[2], vm.PC, 2);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            arg0.Data = arg1 - arg2;

            //ログ出力
            NovelDebug.Log(vm.PC, "[opr]sub [arg0]reg:" + arg0.ToString() + "[arg1-2]num:" + arg1.ToString() + "-" + arg2.ToString());

            yield return null;
        }
    }

    /// mul
    /// 乗算コマンド
    /// 掛け算の結果をレジスタに格納します。
    /// @param reg 代入先レジスタ
    /// @param num 左辺
    /// @param num 右辺
    [NvCommand(id = 7)]
    sealed class NvCmd_Mul : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            NvRegister arg0 = null;
            int arg1 = 0;
            int arg2 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToReg(args[0], vm.PC, 0);
                arg1 = NvConvert.ToInt(args[1], vm.PC, 1);
                arg2 = NvConvert.ToInt(args[2], vm.PC, 2);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            arg0.Data = arg1 * arg2;

            //ログ出力
            NovelDebug.Log(vm.PC, "[opr]add [arg0]reg:" + arg0.ToString() + "[arg1*2]num:" + arg1.ToString() + "*" + arg2.ToString());

            yield return null;
        }
    }
    
    /// div
    /// 割り算コマンド
    /// 割り算の結果をレジスタに格納します。
    /// @param reg 代入先レジスタ
    /// @param num 左辺
    /// @param num 右辺
    [NvCommand(id = 8)]
    sealed class NvCmd_Div : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            NvRegister arg0 = null;
            int arg1 = 0;
            int arg2 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToReg(args[0], vm.PC, 0);
                arg1 = NvConvert.ToInt(args[1], vm.PC, 1);
                arg2 = NvConvert.ToInt(args[2], vm.PC, 2);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            if (arg2 == 0)
            {
                NovelDebug.LogError(vm.PC, "ZeroDivエラーです。計算式を確認してください。");
            }
            else
            {
                arg0.Data = arg1 / arg2;
            }

            //ログ出力
            NovelDebug.Log(vm.PC, "[opr]div [arg0]reg:" + arg0.ToString() + "[arg1+2]num:" + arg1.ToString() + "/" + arg2.ToString());

            yield return null;
        }
    }

    /// echo
    /// エコーコマンド
    /// 標準出力へ指定した文字列を出力します。
    /// 主にデバッグ用のコマンドです。レジスタの値の確認などに使えます。
    /// @param str 出力する文字列
    /// @param reg 文字列に差し込む値（任意の数）
    [NvCommand(id = 9)]
    sealed class NvCmd_Echo0 : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            string arg0 = "";
            List<NvRegister> arg1 = new List<NvRegister>();

            //引数変換
            try
            {
                arg0 = NvConvert.ToStr(args[0], vm.PC, 0);
                foreach (var dat in args.GetRange(1, args.Count - 1))
                    arg1.Add(NvConvert.ToReg(dat, vm.PC, 1));
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            
            //文字列置換
            for(int i=0; i< arg1.Count; i++)
            {
                //置換文字列
                string rep = "%"+i.ToString();
                //置換
                arg0 = arg0.Replace(rep, arg1[i].Data.ToString());
            }

            //レジスタ置換
            var regex = new Regex("@(|pc|sp|sg|tm|ra)[0-9]*");
            var matches = regex.Matches(arg0);
            foreach(Match m in matches)
            {
                var reg = vm.RegToReg(m.Value);
                arg0 = arg0.Replace(m.Value, reg.Data.ToString());
            }

            //出力
            vm.Engine.SS_IO.Print(arg0);

            //ログ出力
            NovelDebug.Log(vm.PC, "[opr]echo [arg0]str:" + arg0.ToString());

            yield return null;
        }
    }

    /// echo
    /// エコーコマンド
    /// 標準出力へ文字列を出力します。
    /// @param str 出力する文字列
    /// @param reg 文字列に差し込む値（任意の数）
    [NvCommand(id = 10)]
    sealed class NvCmd_Echo1 : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            string arg0 = "";

            //引数変換
            try
            {
                arg0 = NvConvert.ToStr(args[0], vm.PC, 0);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //レジスタ置換
            var regex = new Regex("@([0-9]+|pc|sp|sg|tm[0-9]+|ra)", RegexOptions.Compiled);
            var matches = regex.Matches(arg0);
            foreach (Match m in matches)
            {
                var reg = vm.RegToReg(m.Value);
                arg0 = arg0.Replace(m.Value, reg.Data.ToString());
            }

            //処理
            vm.Engine.SS_IO.Print(arg0);

            //ログ出力
            NovelDebug.Log(vm.PC, "[opr]echo [arg0]str:" + arg0.ToString());

            yield return null;
        }
    }
    
    /// comp
    /// 比較コマンド
    /// 値を比較して、結果を符号レジスタへ格納します。
    /// ジャンプ命令と組み合わせて使用します。
    /// @param num 左辺
    /// @param num 右辺
    [NvCommand(id = 11)]
    sealed class NvCmd_Comp : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            int arg0 = 0;
            int arg1 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToInt(args[0], vm.PC, 0);
                arg1 = NvConvert.ToInt(args[1], vm.PC, 1);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            int result = 0;
            if (arg0 == arg1) result = 0;
            else if (arg0 < arg1) result = -1;
            else result = 1;

            vm.SpReg[(int)ENvSpRegType.sg].Data = result;

            //ログ出力
            NovelDebug.Log(vm.PC, "[opr]comp [arg0]num:" + arg0.ToString() + "[arg1]num:" + arg1.ToString());
            yield return null;
        }
    }

    /// jmp
    /// ジャンプコマンド
    /// 無条件でlabelをジャンプ
    /// @param adr ジャンプ先のラベル
    [NvCommand(id = 12)]
    sealed class NvCmd_Jmp : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            int arg0 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToInt(args[0], vm.PC, 0);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            vm.PC = arg0;

            //ログ出力
            NovelDebug.Log(vm.PC, "[opr]jmp [arg0]adr:" + arg0.ToString());

            yield return null;
        }
    }

    /// jzer
    /// ジャンプゼロコマンド
    /// 符号レジスタが0のとき、つまりcomp演算の結果が同じ数値だったとき
    /// ジャンプをするコマンドです。
    /// @param adr ジャンプ先のアドレス
    [NvCommand(id = 13)]
    sealed class NvCmd_JmpZero : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            int arg0 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToInt(args[0], vm.PC, 0);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            if((int)vm.SpReg[(int)ENvSpRegType.sg].Data == 0)
                vm.PC = arg0;

            //ログ出力
            NovelDebug.Log(vm.PC, "[opr]jzer [arg0]adr:" + arg0.ToString());

            yield return null;
        }
    }

    /// jmin
    /// ジャンプマイナスコマンド
    /// 符号レジスタが負の値のとき、つまりcomp演算の＜演算が合格したとき
    /// ジャンプをするコマンドです。
    /// @param adr ジャンプ先のアドレス
    [NvCommand(id = 14)]
    sealed class NvCmd_JmpMin : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            int arg0 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToInt(args[0], vm.PC, 0);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            if ((int)vm.SpReg[(int)ENvSpRegType.sg].Data < 0)
                vm.PC = arg0;

            //ログ出力
            NovelDebug.Log(vm.PC, "[opr]jmin [arg0]adr:" + arg0.ToString());

            yield return null;
        }
    }

    /// jplu
    /// ジャンププラスコマンド
    /// 符号レジスタが正の値のとき、つまりcomp演算の＞演算が合格したとき
    /// ジャンプをするコマンドです。
    /// @param adr ジャンプ先のアドレス
    [NvCommand(id = 15)]
    sealed class NvCmd_JmpPlus : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            int arg0 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToInt(args[0], vm.PC, 0);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            if ((int)vm.SpReg[(int)ENvSpRegType.sg].Data > 0)
                vm.PC = arg0;

            //ログ出力
            NovelDebug.Log(vm.PC, "[opr]jplu [arg0]adr:" + arg0.ToString());

            yield return null;
        }
    }

    /// push
    /// プッシュコマンド
    /// @param reg プッシュするレジスタ
    [NvCommand(id = 16)]
    sealed class NvCmd_PushReg : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            NvRegister arg0 = null;

            //引数変換
            try
            {
                arg0 = NvConvert.ToReg(args[0], vm.PC, 0);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            vm.Stack.Push(arg0.Data);

            //ログ出力
            NovelDebug.Log(vm.PC, "[opr]push [arg0]reg:" + arg0.ToString());

            yield return null;
        }
    }

    /// push
    /// プッシュコマンド
    /// @param num プッシュする数値
    [NvCommand(id = 17)]
    sealed class NvCmd_PushNum : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            int arg0 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToInt(args[0], vm.PC, 0);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            vm.Stack.Push(arg0);

            //ログ出力
            NovelDebug.Log(vm.PC, "[opr]push [arg0]num:" + arg0.ToString());

            yield return null;
        }
    }

    /// push
    /// プッシュコマンド
    /// @param num プッシュする文字列
    [NvCommand(id = 18)]
    sealed class NvCmd_PushStr : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            string arg0 = "";

            //引数変換
            try
            {
                arg0 = NvConvert.ToStr(args[0], vm.PC, 0);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            vm.Stack.Push(arg0);

            //ログ出力
            NovelDebug.Log(vm.PC, "[opr]push [arg0]str:" + arg0.ToString());

            yield return null;
        }
    }
    
    /// pop
    /// ポップコマンド
    /// @param reg 代入先レジスタ
    [NvCommand(id = 19)]
    sealed class NvCmd_Pop : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            NvRegister arg0 = null;

            //引数変換
            try
            {
                arg0 = NvConvert.ToReg(args[0], vm.PC, 0);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            var data = vm.Stack.Pop();
            arg0.Data = data;

            //ログ出力
            NovelDebug.Log(vm.PC, "[opr]pop [arg0]reg:" + arg0.ToString());

            yield return null;
        }
    }
    
    /// async
    /// 同期コマンド（同期開始）
    /// awaitまでに入力されたコマンドを同期します。
    [NvCommand(id = 20)]
    sealed class NvCmd_Async : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            //処理
            vm.IsSync = true;

            //ログ出力
            NovelDebug.Log(vm.PC, "[opr]async");

            yield return null;
        }
    }

    /// await
    /// 同期コマンド（同期まち）
    /// 同期用に積まれたメソッドを一気に消化する
    [NvCommand(id = 21)]
    sealed class NvCmd_Await : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            //処理
            bool isEnd = false;
            if (vm.IsSync)
            {
                yield return vm.Engine.StartParallelCoroutine(vm.SyncMethod, () => { isEnd = true; });
            }
            else isEnd = true;

            //ログ出力
            NovelDebug.Log(vm.PC, "[opr]await");

            while (!isEnd) yield return null;

            vm.IsSync = false;
            yield return null;
        }
    }

    /// call
    /// コールコマンド
    /// @param adr ジャンプ先アドレス
    [NvCommand(id = 22)]
    sealed class NvCmd_Call : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            int arg0 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToInt(args[0], vm.PC, 0);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理

            //PCを保存
            vm.SpReg[(int)ENvSpRegType.ra].Data = vm.PC;

            //ジャンプ
            vm.PC = arg0;

            //ログ出力
            NovelDebug.Log(vm.PC, "[opr]call [arg0]adr:" + arg0.ToString());

            yield return null;
        }
    }

    /// return
    /// リターンコマンド
    [NvCommand(id = 23)]
    sealed class NvCmd_Return : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            //処理
            //リターンアドレスに保存したPCを復帰
            vm.PC = (int)vm.SpReg[(int)ENvSpRegType.ra].Data;

            //ログ出力
            NovelDebug.Log(vm.PC, "[opr]return");

            yield return null;
        }
    }

    /// exit
    /// 終了コマンド
    [NvCommand(id = 24)]
    sealed class NvCmd_Exit : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            //処理
            vm.IsEnd = true;

            //ログ出力
            NovelDebug.Log(vm.PC, "[opr]exit");

            yield return null;
        }
    }

    /// assert
    /// アサートコマンド
    /// スクリプト側からゲームプログラムへエラーを通知できる唯一の処理
    [NvCommand(id = 25)]
    sealed class NvCmd_Assert : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            //処理
            NovelDebug.LogError(vm.PC, "[opr]assert");

            yield return null;
        }
    }

    /// mset
    /// システムメモリセットコマンド
    /// ゲーム側のメモリへメモリセットする処理
    /// @param num セット先のindex
    /// @param num セットする値
    [NvCommand(id = 26)]
    sealed class NvCmd_SystemMemSet0 : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            int arg0 = 0;
            int arg1 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToInt(args[0], vm.PC, 0);
                arg1 = NvConvert.ToInt(args[1], vm.PC, 1);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            vm.Engine.SystemMemory[arg0] = arg1;

            //出力
            NovelDebug.Log(vm.PC, "[opr]mset [arg0]:" + arg0.ToString() + " [arg1]:" + arg1.ToString());

            yield return null;
        }
    }
    
    /// mset
    /// システムメモリセットコマンド
    /// ゲーム側のメモリへメモリセットする処理
    /// @param num セット先のindex
    /// @param num セットする値
    [NvCommand(id = 27)]
    sealed class NvCmd_SystemMemSet1 : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            int arg0 = 0;
            string arg1 = "";

            //引数変換
            try
            {
                arg0 = NvConvert.ToInt(args[0], vm.PC, 0);
                arg1 = NvConvert.ToStr(args[1], vm.PC, 1);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            vm.Engine.SystemMemory[arg0] = arg1;

            //出力
            NovelDebug.Log(vm.PC, "[opr]mset [arg0]num:" + arg0.ToString() + " [arg1]str:" + arg1);

            yield return null;
        }
    }

    /// mget
    /// システムメモリゲットコマンド
    /// ゲーム側のメモリからノベルエンジンのレジスタへ値を移す処理
    /// @param reg 格納先のローカルのレジスタ番号
    /// @param num 取得先のシステムメモリインデックス
    [NvCommand(id = 28)]
    sealed class NvCmd_SystemMemGet : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            NvRegister arg0 = null;
            int arg1 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToReg(args[0], vm.PC, 0);
                arg1 = NvConvert.ToInt(args[1], vm.PC, 1);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            arg0.Data = vm.Engine.SystemMemory[arg1];

            //出力
            NovelDebug.Log(vm.PC, "[opr]mget [arg0]reg:" + arg0.ToString() + " [arg1]num:" + arg1.ToString());

            yield return null;
        }
    }

    /// _name
    /// 名前表示コマンド
    /// ノベルにおける、話者を表示するコマンド
    /// @param str 話者名
    [NvCommand(id = 29)]
    sealed class NvCmd_NovelName : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            string arg0 = "";

            //引数変換
            try
            {
                arg0 = NvConvert.ToStr(args[0], vm.PC, 0);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            if (arg0 == "null") arg0 = "";
            vm.Engine.SS_Text.ChangeName(arg0);

            //出力
            NovelDebug.Log(vm.PC, "[opr]_name [arg0]str:" + arg0.ToString());

            yield return null;
        }
    }

    /// _text
    /// テキスト表示コマンド
    /// ノベルにおいて、テキストを表示するコマンド
    /// 基本的に行単位。命令のたびに開業され、文章が追加される。
    /// newpageコマンドを呼び出すことで改頁される
    /// @param str 格納先のローカルのレジスタ番号
    [NvCommand(id = 30)]
    sealed class NvCmd_NovelText : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            string arg0 = "";

            //引数変換
            try
            {
                arg0 = NvConvert.ToStr(args[0], vm.PC, 0);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            yield return vm.Engine.SS_Text.ShowText(arg0);

            //出力
            NovelDebug.Log(vm.PC, "[opr]_text [arg0]str:" + arg0.ToString());

            yield return null;
        }
    }

    /// _newpage
    /// 改ページコマンド
    /// ノベルにおいて、改頁するコマンド
    [NvCommand(id = 31)]
    sealed class NvCmd_NovelNewPage : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            //処理
            yield return vm.Engine.SS_Text.NewPage();

            //出力
            NovelDebug.Log(vm.PC, "[opr]_newpage");

            yield return null;
        }
    }

    /// _voice
    /// ボイス再生コマンド
    /// @param str ファイル名
    [NvCommand(id = 32)]
    sealed class NvCmd_PlayVoice : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            string arg0 = "";

            //引数変換
            try
            {
                arg0 = NvConvert.ToStr(args[0], vm.PC, 0);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            vm.Engine.SS_Sound.PlayVoice(vm.PC, arg0);

            //出力
            NovelDebug.Log(vm.PC, "[opr]_voice [arg0]str:" + arg0.ToString());

            yield return null;
        }
    }
    
    /// wait
    /// 待機コマンド
    /// @param num 時間（ms）
    [NvCommand(id = 33)]
    sealed class NvCmd_Wait : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            int arg0 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToInt(args[0], vm.PC, 0);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //出力
            NovelDebug.Log(vm.PC, "[opr]wait [arg0]num:" + arg0.ToString());

            yield return new WaitForSeconds(arg0 / 1000.0f);
        }
    }

    /// new
    /// インスタンス生成コマンド
    /// @param str プレハブ名
    /// @param str 親オブジェクト名
    /// @param reg ハンドルの格納先
    [NvCommand(id = 64)]
    sealed class NvCmd_New : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            string arg0 = "";
            string arg1 = "";
            NvRegister arg2 = null;

            //引数変換
            try
            {
                arg0 = NvConvert.ToStr(args[0], vm.PC, 0);
                arg1 = NvConvert.ToStr(args[1], vm.PC, 1);
                arg2 = NvConvert.ToReg(args[2], vm.PC, 2);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            arg2.Data = vm.Engine.SS_Graphics.NewPrefab(vm.PC, arg0, arg1);

            //出力
            NovelDebug.Log(vm.PC, "[opr]new [arg0]str:" + arg0.ToString() + "[arg1]str:" + arg1.ToString() + "[arg2]reg:" + arg2.Data.ToString());

            yield return null;
        }
    }

    /// delete
    /// インスタンス削除コマンド
    /// @param reg 画像のハンドル
    [NvCommand(id = 65)]
    sealed class NvCmd_Delete : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            NvRegister arg0 = null;

            //引数変換
            try
            {
                arg0 = NvConvert.ToReg(args[0], vm.PC, 0);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            vm.Engine.SS_Graphics.DeletePrefab((int)arg0.Data);

            //出力
            NovelDebug.Log(vm.PC, "[opr]delete [arg0]reg:" + arg0.Data.ToString());

            yield return null;
        }
    }

    /// show
    /// 画像を表示する
    /// @param reg 画像のハンドル
    /// @param num x座標
    /// @param num y座標
    /// @param num 重ね順
    /// @param num 時間
    [NvCommand(id = 66, syncEnable = true)]
    sealed class NvCmd_Show0 : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            NvRegister arg0 = null;
            int arg1 = 0;
            int arg2 = 0;
            int arg3 = 0;
            int arg4 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToReg(args[0], vm.PC, 0);
                arg1 = NvConvert.ToInt(args[1], vm.PC, 1);
                arg2 = NvConvert.ToInt(args[2], vm.PC, 2);
                arg3 = NvConvert.ToInt(args[3], vm.PC, 3);
                arg4 = NvConvert.ToInt(args[4], vm.PC, 4);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            yield return vm.Engine.SS_Graphics.ShowPrefab(vm.PC,(int)arg0.Data, new Vector2(arg1,arg2), arg3, arg4/1000.0f);

            //出力
            NovelDebug.Log(vm.PC, "[opr]show [arg0]reg:" + arg0.Data.ToString() + " [arg1]int:" + arg1.ToString()
                + "[arg2]int:" + arg2.ToString() + "[arg3]int:" + arg3.ToString() + "[arg4]int:" + arg4.ToString());

            yield return null;
        }
    }

    /// show
    /// 画像を表示する
    /// @param reg 画像のハンドル
    /// @param num 時間
    [NvCommand(id = 67, syncEnable = true)]
    sealed class NvCmd_Show1 : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            NvRegister arg0 = null;
            int arg1 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToReg(args[0], vm.PC, 0);
                arg1 = NvConvert.ToInt(args[1], vm.PC, 1);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            yield return vm.Engine.SS_Graphics.ShowPrefab(vm.PC, (int)arg0.Data, arg1 / 1000.0f);

            //出力
            NovelDebug.Log(vm.PC, "[opr]show [arg0]reg:" + arg0.Data.ToString() + "[arg1]int:" + arg1.ToString());

            yield return null;
        }
    }

    /// hide
    /// 画像を表示する
    /// @param reg 画像のハンドル
    /// @param num 時間
    [NvCommand(id = 68, syncEnable = true)]
    sealed class NvCmd_Hide : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            NvRegister arg0 = null;
            int arg1 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToReg(args[0], vm.PC, 0);
                arg1 = NvConvert.ToInt(args[1], vm.PC, 1);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            yield return vm.Engine.SS_Graphics.HidePrefab(vm.PC, (int)arg0.Data, arg1 / 1000.0f);

            //出力
            NovelDebug.Log(vm.PC, "[opr]hide [arg0]reg:" + arg0.Data.ToString() + "[arg1]int:" + arg1.ToString());

            yield return null;
        }
    }

    /// scale
    /// 画像を表示する
    /// @param reg 画像のハンドル
    /// @param num x倍率
    /// @param num y倍率
    /// @param num 時間
    [NvCommand(id = 69, syncEnable = true)]
    sealed class NvCmd_Scale : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            NvRegister arg0 = null;
            int arg1 = 0;
            int arg2 = 0;
            int arg3 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToReg(args[0], vm.PC, 0);
                arg1 = NvConvert.ToInt(args[1], vm.PC, 1);
                arg2 = NvConvert.ToInt(args[2], vm.PC, 2);
                arg3 = NvConvert.ToInt(args[3], vm.PC, 3);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            yield return vm.Engine.SS_Graphics.Scale(vm.PC, (int)arg0.Data, new Vector2(arg1/100.0f, arg2/100.0f), arg3/1000.0f);

            //出力
            NovelDebug.Log(vm.PC, "[opr]scale [arg0]reg:" + arg0.Data.ToString() +
                "[arg1]num:" + arg1.ToString() + "[arg2]num:" + arg2.ToString() + "[arg3]num:" + arg3.ToString());

            yield return null;
        }
    }

    /// move
    /// 画像を移動する
    /// @param reg 画像のハンドル
    /// @param num x差分
    /// @param num y差分
    /// @param num 時間
    [NvCommand(id = 70, syncEnable = true)]
    sealed class NvCmd_Move : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            NvRegister arg0 = null;
            int arg1 = 0;
            int arg2 = 0;
            int arg3 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToReg(args[0], vm.PC, 0);
                arg1 = NvConvert.ToInt(args[1], vm.PC, 1);
                arg2 = NvConvert.ToInt(args[2], vm.PC, 2);
                arg3 = NvConvert.ToInt(args[3], vm.PC, 3);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            yield return vm.Engine.SS_Graphics.Move(vm.PC, (int)arg0.Data, new Vector2(arg1, arg2), arg3 / 1000.0f);

            //出力
            NovelDebug.Log(vm.PC, "[opr]move [arg0]reg:" + arg0.Data.ToString() +
                "[arg1]num:" + arg1.ToString() + "[arg2]num:" + arg2.ToString() +
                "[arg3]num:" + arg3.ToString());

            yield return null;
        }
    }

    /// rotate
    /// 画像を回転する
    /// @param reg 画像のハンドル
    /// @param num 回転角
    /// @param num 時間
    [NvCommand(id = 71, syncEnable = true)]
    sealed class NvCmd_Rot : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            NvRegister arg0 = null;
            int arg1 = 0;
            int arg2 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToReg(args[0], vm.PC, 0);
                arg1 = NvConvert.ToInt(args[1], vm.PC, 1);
                arg2 = NvConvert.ToInt(args[2], vm.PC, 2);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            yield return vm.Engine.SS_Graphics.Rotate(vm.PC, (int)arg0.Data, arg1, arg2 / 1000.0f);

            //出力
            NovelDebug.Log(vm.PC, "[opr]rotate [arg0]reg:" + arg0.Data.ToString() +
                "[arg1]num:" + arg1.ToString() + "[arg2]num:" + arg2.ToString());

            yield return null;
        }
    }

    /// fx_fade
    /// 画面をフェードする
    /// @param str 色
    /// @param num 時間
    [NvCommand(id = 72, syncEnable = true)]
    sealed class NvCmd_FX_Fade : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            string arg0 = null;
            int arg1 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToStr(args[0], vm.PC, 0);
                arg1 = NvConvert.ToInt(args[1], vm.PC, 1);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            Debug.Log("fx_fadeはまだじっそうしてません");

            //出力
            NovelDebug.Log(vm.PC, "[opr]fx_fade [arg0]str:" + arg0 +
                "[arg1]num:" + arg1.ToString());

            yield return null;
        }
    }

    /// fx_clear
    /// 画面効果をクリアする
    /// @param num 時間
    [NvCommand(id = 73, syncEnable = true)]
    sealed class NvCmd_FX_Clear : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            int arg0 = 0;

            //引数変換
            try
            {
                arg0 = NvConvert.ToInt(args[0], vm.PC, 0);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            Debug.Log("fx_clearはまだじっそうしてません");

            //出力
            NovelDebug.Log(vm.PC, "[opr]fx_clear [arg0]str:" + arg0.ToString());

            yield return null;
        }
    }

    /// bgm
    /// BGMを再生する
    /// @param str ファイル名
    [NvCommand(id = 74)]
    sealed class NvCmd_BGM : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            string arg0 = null;

            //引数変換
            try
            {
                arg0 = NvConvert.ToStr(args[0], vm.PC, 0);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            vm.Engine.SS_Sound.PlayBGM(vm.PC, arg0);

            //出力
            NovelDebug.Log(vm.PC, "[opr]bgm [arg0]str:" + arg0);

            yield return null;
        }
    }

    /// se
    /// SEを再生する
    /// @param str ファイル名
    [NvCommand(id = 75)]
    sealed class NvCmd_SE : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            string arg0 = null;

            //引数変換
            try
            {
                arg0 = NvConvert.ToStr(args[0], vm.PC, 0);
            }
            catch (ArgumentOutOfRangeException)
            {
                NovelDebug.LogError(vm.PC, "引数の数が足りません。");
            }

            //処理
            vm.Engine.SS_Sound.PlaySE(vm.PC, arg0);

            //出力
            NovelDebug.Log(vm.PC, "[opr]se [arg0]str:" + arg0);

            yield return null;
        }
    }

    /// stopbgm
    /// BGMを停止する
    /// @param str ファイル名
    [NvCommand(id = 76)]
    sealed class NvCmd_StopBGM : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            //処理
            vm.Engine.SS_Sound.StopBGM();

            //出力
            NovelDebug.Log(vm.PC, "[opr]stopbgm");

            yield return null;
        }
    }

    /// win_open
    /// windowを開く
    [NvCommand(id = 77)]
    sealed class NvCmd_WinOpen : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            //処理
            vm.Engine.SS_Text.WinOpen();

            //出力
            NovelDebug.Log(vm.PC, "[opr]win_open");

            yield return null;
        }
    }

    /// win_close
    /// windowを閉じる
    [NvCommand(id = 78)]
    sealed class NvCmd_WinClose : NvCommand
    {
        public override IEnumerator Process(NovelVM vm, List<object> args)
        {
            //処理
            vm.Engine.SS_Text.WinClose();

            //出力
            NovelDebug.Log(vm.PC, "[opr]win_close");

            yield return null;
        }
    }
}