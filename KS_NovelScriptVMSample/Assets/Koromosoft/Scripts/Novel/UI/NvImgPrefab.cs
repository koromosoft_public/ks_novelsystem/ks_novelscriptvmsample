﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class NvImgPrefab : MonoBehaviour
{
    private List<Image> _Img = null;

    private void Awake()
    {
        _Img = GetComponentsInChildren<Image>().ToList();
    }

    //透明度をセット
    public void SetAlpha(float a)
    {
        foreach(var i in _Img)
        {
            var c = i.color;
            c.a = a;
            i.color = c;
        }
    }
}
